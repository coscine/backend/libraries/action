﻿using Coscine.Configuration;
using Coscine.Database.DataModel;

namespace Coscine.Action.EventArgs
{
    public class ResourceEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }
        public User ResourceOwner { get; set; }
        public Resource Resource { get; set; }

        public ResourceEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public ResourceEventArgs(IConfiguration configuration) : this(configuration, new object[0])
        {
        }
    }
}
