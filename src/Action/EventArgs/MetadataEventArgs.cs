﻿using Coscine.Configuration;
using System.Collections.Generic;

namespace Coscine.Action.EventArgs
{
    public class MetadataEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }

        public string GraphName { get; set; }

        public List<string> DefaultLanguages = new List<string> { "en", "de" };

        public MetadataEventArgs(IConfiguration configuration, string graphName, object[] args) : base(args)
        {
            Configuration = configuration;
            GraphName = graphName;
        }

        public MetadataEventArgs(IConfiguration configuration, string graphName) : this(configuration, graphName, System.Array.Empty<object>())
        {
        }
    }
}
