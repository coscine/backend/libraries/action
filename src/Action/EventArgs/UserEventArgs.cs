﻿using Coscine.Configuration;
using Coscine.Database.DataModel;

namespace Coscine.Action.EventArgs
{
    public class UserEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }

        public User User { get; set; }
        public Project Project { get; set; }
        public Role Role { get; set; }

        public UserEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public UserEventArgs(IConfiguration configuration) : this(configuration, System.Array.Empty<object>())
        {
        }
    }
}
