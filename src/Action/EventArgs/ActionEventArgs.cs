﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Action.EventArgs
{
    public abstract class ActionEventArgs : System.EventArgs
    {
        public object[] Args { get; set; }
        public bool SilentMode { get; set; } = false;

        protected ActionEventArgs()
        {
        }

        protected ActionEventArgs(object[] args)
        {
            Args = args;
        }
    }
}
