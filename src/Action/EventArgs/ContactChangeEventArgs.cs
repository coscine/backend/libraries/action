﻿using Coscine.Configuration;
using Coscine.Database.ReturnObjects;

namespace Coscine.Action.EventArgs
{
    public class ContactChangeEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }
        public ContactChangeObject ContactInformation { get; set; }

        public ContactChangeEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public ContactChangeEventArgs(IConfiguration configuration) : this(configuration, System.Array.Empty<object>())
        {
        }
    }
}
