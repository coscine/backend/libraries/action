﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Newtonsoft.Json.Linq;

namespace Coscine.Action.EventArgs
{
    public class PIDEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }
        public User Requester { get; set; }
        public Resource Resource { get; set; }
        public Project Project { get; set; }
        public JObject Placeholder { get; set; }
        public bool SentCopy { get; set; }

        public PIDEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public PIDEventArgs(IConfiguration configuration) : this(configuration, System.Array.Empty<object>())
        {
        }
    }
}
