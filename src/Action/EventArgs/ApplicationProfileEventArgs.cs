﻿using Coscine.Configuration;
using Coscine.Database.DataModel;

namespace Coscine.Action.EventArgs
{
    public class ApplicationProfileEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }
        public User RequestOwner { get; set; }
        public string ApplicationProfileName { get; set; }
        public string MergeRequestURL { get; set; }

        public ApplicationProfileEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public ApplicationProfileEventArgs(IConfiguration configuration) : this(configuration, new object[0])
        {
        }
    }
}
