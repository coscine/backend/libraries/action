﻿using Coscine.Configuration;
using Coscine.Database.DataModel;

namespace Coscine.Action.EventArgs
{
    public class ProjectEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }
        public User ProjectOwner { get; set; }
        public Project Project { get; set; }

        public ProjectEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public ProjectEventArgs(IConfiguration configuration) : this(configuration, System.Array.Empty<object>())
        {
        }
    }
}
