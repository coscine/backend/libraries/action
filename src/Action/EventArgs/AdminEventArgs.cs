﻿using Coscine.Configuration;
using System;

namespace Coscine.Action.EventArgs
{
    public class AdminEventArgs : ActionEventArgs
    {
        public IConfiguration Configuration { get; set; }
        public Guid ProjectId { get; set; }

        public AdminEventArgs(IConfiguration configuration, object[] args) : base(args)
        {
            Configuration = configuration;
        }

        public AdminEventArgs(IConfiguration configuration) : this(configuration, Array.Empty<object>())
        {
        }
    }
}
