﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IUserAction : IAction
    {
        void OnUserSet(UserEventArgs userEventArgs);
        void OnUserDelete(UserEventArgs userEventArgs);
    }
}
