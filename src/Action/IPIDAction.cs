﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IPIDAction : IAction
    {
        void OnPIDOwnerContact(PIDEventArgs pidEventArgs);
    }
}
