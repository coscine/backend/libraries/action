﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IAdminAction : IAction
    {
        void OnQutaChanged(AdminEventArgs pidEventArgs);
    }
}
