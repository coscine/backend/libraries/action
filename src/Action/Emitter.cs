﻿using Coscine.Action.EventArgs;
using Coscine.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Action
{
    public class Emitter
    {
        private readonly List<IProjectAction> projectActions = new List<IProjectAction>();
        private readonly List<IUserAction> userActions = new List<IUserAction>();
        private readonly List<IResourceAction> resourceActions = new List<IResourceAction>();
        private readonly List<IPIDAction> pidActions = new List<IPIDAction>();
        private readonly List<IAdminAction> adminActions = new List<IAdminAction>();
        private readonly List<IContactChangeAction> contactChangeActions = new List<IContactChangeAction>();
        private readonly List<IMetadataAction> metadataActions = new List<IMetadataAction>();
        private readonly List<IApplicationProfileAction> applicationProfileActions = new List<IApplicationProfileAction>();

        public IConfiguration Configuration { get; }

        public Emitter(IConfiguration configuration)
        {
            Configuration = configuration;
            RegisterLocalImplementations();
        }

        private void RegisterLocalImplementations()
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            var pa = InstantiateAllImplementations<IProjectAction>(assembly);
            projectActions.AddRange(pa);

            var ua = InstantiateAllImplementations<IUserAction>(assembly);
            userActions.AddRange(ua);

            var ca = InstantiateAllImplementations<IContactChangeAction>(assembly);
            contactChangeActions.AddRange(ca);

            var ra = InstantiateAllImplementations<IResourceAction>(assembly);
            resourceActions.AddRange(ra);

            var pida = InstantiateAllImplementations<IPIDAction>(assembly);
            pidActions.AddRange(pida);

            var adminA = InstantiateAllImplementations<IAdminAction>(assembly);
            adminActions.AddRange(adminA);

            var metadataA = InstantiateAllImplementations<IMetadataAction>(assembly);
            metadataActions.AddRange(metadataA);

            var apa = InstantiateAllImplementations<IApplicationProfileAction>(assembly);
            applicationProfileActions.AddRange(apa);
        }

        private static IEnumerable<T> InstantiateAllImplementations<T>(System.Reflection.Assembly assembly) where T : IAction
        {
            return from t in assembly.DefinedTypes
                   where t.ImplementedInterfaces.Contains(typeof(T))
                   let i = (T)t.GetConstructor(Array.Empty<Type>()).Invoke(Array.Empty<object>())
                   where i.Enabled
                   orderby i.Priority ascending
                   select i;
        }

        public void EmitProjectCreate(ProjectEventArgs args)
        {
            foreach (var action in projectActions)
            {
                action.OnProjectCreate(args);
            }
        }

        public void EmitProjectDelete(ProjectEventArgs args)
        {
            foreach (var action in projectActions)
            {
                action.OnProjectDelete(args);
            }
        }

        public void EmitUserAdd(UserEventArgs args)
        {
            foreach (var action in userActions)
            {
                action.OnUserSet(args);
            }
        }

        public void EmitUserRemove(UserEventArgs args)
        {
            foreach (var action in userActions)
            {
                action.OnUserDelete(args);
            }
        }

        public void EmitContactChange(ContactChangeEventArgs args)
        {
            foreach (var action in contactChangeActions)
            {
                action.OnContactChange(args);
            }
        }

        public void EmitResourceCreate(ResourceEventArgs args)
        {
            foreach (var action in resourceActions)
            {
                action.OnResourceCreate(args);
            }
        }

        public void EmitResourceDelete(ResourceEventArgs args)
        {
            foreach (var action in resourceActions)
            {
                action.OnResourceDelete(args);
            }
        }

        public void EmitPIDOwnerContact(PIDEventArgs args)
        {
            foreach (var action in pidActions)
            {
                action.OnPIDOwnerContact(args);
            }
        }

        public void EmitQuotaChanged(AdminEventArgs args)
        {
            foreach (var action in adminActions)
            {
                action.OnQutaChanged(args);
            }
        }

        public void EmitMetadataCreate(MetadataEventArgs args)
        {
            foreach (var action in metadataActions)
            {
                action.OnMetadataCreate(args);
            }
        }

        public void EmitMetadataUpdate(MetadataEventArgs args)
        {
            foreach (var action in metadataActions)
            {
                action.OnMetadataUpdate(args);
            }
        }

        public void EmitMetadataDelete(MetadataEventArgs args)
        {
            foreach (var action in metadataActions)
            {
                action.OnMetadataDelete(args);
            }
        }

        public void EmitApplicationProfileRequest(ApplicationProfileEventArgs args)
        {
            foreach (var a in applicationProfileActions)
            {
                a.OnApplicationProfileRequest(args);
            }
        }
    }
}
