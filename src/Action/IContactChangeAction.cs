﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IContactChangeAction : IAction
    {
        void OnContactChange(ContactChangeEventArgs contactChangeEventArgs);
    }
}
