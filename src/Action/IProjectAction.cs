﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IProjectAction : IAction
    {
        void OnProjectCreate(ProjectEventArgs projectEventArgs);
        void OnProjectDelete(ProjectEventArgs projectEventArgs);
    }
}
