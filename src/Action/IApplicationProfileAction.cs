﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IApplicationProfileAction : IAction
    {
        void OnApplicationProfileRequest(ApplicationProfileEventArgs applicationProfileEventArgs);
    }
}
