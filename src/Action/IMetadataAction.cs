﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IMetadataAction : IAction
    {
        void OnMetadataCreate(MetadataEventArgs args);

        void OnMetadataUpdate(MetadataEventArgs args);

        void OnMetadataDelete(MetadataEventArgs args);
    }
}
