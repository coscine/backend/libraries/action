﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Action.Utils
{
    public class NotificationBusUtil
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        public static void Send(IConfiguration configuration, string action, JArray users, string projectId, JObject body)
        {
            SendNotification(configuration, action, users, projectId, body, false).Wait();
        }

        public static async void SendAsync(IConfiguration configuration, string action, JArray users, string projectId, JObject body)
        {
            await SendNotification(configuration, action, users, projectId, body, true);
        }

        private static async Task SendNotification(IConfiguration configuration, string action, JArray users, string projectId, JObject body, bool sync)
        {
            var uri = $"http://localhost:{configuration.GetStringAndWait("coscine/apis/Coscine.Api.NotificationBus/port")}/NotificationBus/send";
            if (!sync)
            {
                uri += "Async";
            }
            body["action"] = action;

            if (projectId != null)
            {
                body["projectId"] = projectId;
            }

            body["users"] = users;

            HttpContent content = new StringContent(body.ToString(), Encoding.UTF8, "application/json");
            await _httpClient.PostAsync(uri, content);
        }

        public static JArray GetUserList(User user)
        {
            return new JArray
            {
                JObject.FromObject(user)
            };
        }

        public static Guid GetProjectId(Guid resourceId)
        {
            var projectResourceModel = new ProjectResourceModel();
            return (Guid)projectResourceModel.GetProjectForResource(resourceId);
        }

        public static JArray GetAllUsersForResourceId(Guid resourceId)
        {
            return GetAllUsersForProjectId(GetProjectId(resourceId));
        }

        public static JArray GetAllOwnersAndCreatorForResourceId(Guid resourceId)
        {
            var owners = GetAllOwnersForProjectId(GetProjectId(resourceId)).ToList();
            var resource = new ResourceModel().GetById(resourceId);
            if (resource.Creator.HasValue)
            {
                if (!owners.Any((owner) => owner.Id == resource.Creator.Value))
                {
                    var creator = new UserModel().GetById(resource.Creator.Value);
                    owners.Add(creator);
                }
            }
            return JArray.FromObject(owners);
        }

        public static JArray GetAllUsersForProjectId(Guid projectId)
        {
            var users = new UserModel().GetAllWhere((user) =>
            (
               from projectRole in user.ProjectRoles
               where projectRole.ProjectId == projectId
               select projectRole).Any()
            );

            return JArray.FromObject(users.ToList());
        }

        public static IEnumerable<User> GetAllOwnersForProjectId(Guid projectId)
        {
            return new UserModel().GetAllWhere((user) =>
            (
               from projectRole in user.ProjectRoles
               where projectRole.ProjectId == projectId
               && projectRole.Role.DisplayName == "Owner"
               select projectRole).Any()
            );
        }

        public static JArray GetAllOwnersForProjectIdPacked(Guid projectId)
        {
            return JArray.FromObject(GetAllOwnersForProjectId(projectId).ToList());
        }
    }
}
