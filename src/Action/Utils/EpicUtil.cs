using Coscine.Configuration;
using Coscine.ProxyApi.Utils;
using System.Collections.Generic;

namespace Coscine.Action.Utils
{
    public class EpicUtil
    {
        private static List<EpicData> GetPIDValues(IConfiguration configuration)
        {
            var _subpath = "/pid/?pid={{pid}}"; // Defines the subpath where the user will be forwarded to
            var baseUrl = configuration.GetStringAndWait("coscine/local/app/additional/url", "https://coscine.rwth-aachen.de");
            var url = new EpicData
            {
                Type = "URL",
                ParsedData = configuration.GetStringAndWait("coscine/global/epic/pid/url", baseUrl + _subpath)
            };
            var metaurl = new EpicData
            {
                Type = "METAURL",
                ParsedData = configuration.GetStringAndWait("coscine/global/epic/pid/metaurl", baseUrl + _subpath)
            };
            var dataurl = new EpicData
            {
                Type = "DATAURL",
                ParsedData = configuration.GetStringAndWait("coscine/global/epic/pid/dataurl", baseUrl + _subpath)
            };

            return new List<EpicData>
            {
                url,
                metaurl,
                dataurl
            };
        }

        public static EpicData CreatePID(string suffix, IConfiguration configuration)
        {
            var epicClient = GetEpicClient(configuration);

            var list = GetPIDValues(configuration);

            return epicClient.Create(list, suffix);
        }

        public static EpicData UpdatePID(string suffix, IConfiguration configuration)
        {
            var epicClient = GetEpicClient(configuration);

            var list = GetPIDValues(configuration);

            return epicClient.Update(suffix, list);
        }

        public static void DeletePID(string suffix, IConfiguration configuration)
        {
            var epicClient = GetEpicClient(configuration);
            epicClient.Delete(suffix);
        }

        private static EpicClient GetEpicClient(IConfiguration configuration)
        {
            var epicUrl = configuration.GetStringAndWait("coscine/global/epic/url");
            var epicPrefix = configuration.GetStringAndWait("coscine/global/epic/prefix");
            var epicUser = configuration.GetStringAndWait("coscine/global/epic/user");
            var epicPassword = configuration.GetStringAndWait("coscine/global/epic/password");

            return new EpicClient(epicUrl, epicPrefix, epicUser, epicPassword);
        }
    }
}
