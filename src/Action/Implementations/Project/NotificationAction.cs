using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Newtonsoft.Json.Linq;

namespace Coscine.Action.Implementations.Project
{
    internal class NotificationAction : IProjectAction
    {
        public int Priority { get { return 3; } }
        public bool Enabled { get { return true; } }

        public void OnProjectCreate(ProjectEventArgs projectEventArgs)
        {
            if (!projectEventArgs.SilentMode)
            {
                NotificationBusUtil.SendAsync(projectEventArgs.Configuration, "project_created", NotificationBusUtil.GetUserList(projectEventArgs.ProjectOwner), projectEventArgs.Project.Id.ToString(), new JObject());
            }
        }

        public void OnProjectDelete(ProjectEventArgs projectEventArgs)
        {
            if (!projectEventArgs.SilentMode)
            {
                NotificationBusUtil.SendAsync(projectEventArgs.Configuration, "project_deleted", NotificationBusUtil.GetAllUsersForProjectId(projectEventArgs.Project.Id), null, new JObject());
            }
        }
    }
}
