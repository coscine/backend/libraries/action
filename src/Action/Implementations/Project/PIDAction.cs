using Coscine.Action.EventArgs;
using Coscine.Action.Utils;

namespace Coscine.Action.Implementations.Project
{
    internal class PIDAction : IProjectAction
    {
        public int Priority { get { return 0; } }
        public bool Enabled { get { return true; } }

        public void OnProjectCreate(ProjectEventArgs projectEventArgs)
        {
            EpicUtil.CreatePID(projectEventArgs.Project.Id.ToString(), projectEventArgs.Configuration);
        }

        public void OnProjectDelete(ProjectEventArgs projectEventArgs)
        {
            // A PID is not intended to be ever deleted
        }
    }
}
