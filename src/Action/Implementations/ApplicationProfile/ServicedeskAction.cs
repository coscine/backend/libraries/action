﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Coscine.Action.Implementations.ApplicationProfile
{
    internal class ServicedeskAction : IApplicationProfileAction
    {
        public static string ServicedeskEmail { get; } = "servicedesk@itc.rwth-aachen.de";
        public int Priority { get { return 1; } }
        public bool Enabled { get { return true; } }

        public void OnApplicationProfileRequest(ApplicationProfileEventArgs applicationProfileEventArgs)
        {
            if (!applicationProfileEventArgs.SilentMode)
            {
                var args = new JObject()
                {
                    ["Args"] = new JObject()
                    {
                        ["placeholder"] = new JObject()
                        {
                            ["applicationProfileName"] = applicationProfileEventArgs.ApplicationProfileName,
                            ["mergeRequestURL"] = applicationProfileEventArgs.MergeRequestURL
                        }
                    }
                };

                NotificationBusUtil.SendAsync(applicationProfileEventArgs.Configuration, "request_applicationprofile_servicedesk_notification", JArray.FromObject(new List<string>() { ServicedeskEmail }), null, args);
            }
        }
    }
}
