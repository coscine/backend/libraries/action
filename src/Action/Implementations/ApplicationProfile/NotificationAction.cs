﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Coscine.Action.Implementations.ApplicationProfile
{
    public class NotificationAction : IApplicationProfileAction
    {
        public int Priority { get { return 1; } }
        public bool Enabled { get { return true; } }

        public void OnApplicationProfileRequest(ApplicationProfileEventArgs applicationProfileEventArgs)
        {
            if (!applicationProfileEventArgs.SilentMode)
            {
                var args = new JObject()
                {
                    ["Args"] = new JObject()
                    {
                        ["placeholder"] = new JObject()
                        {
                            ["applicationProfileName"] = applicationProfileEventArgs.ApplicationProfileName, 
                            ["mergeRequestURL"] = applicationProfileEventArgs.MergeRequestURL
                        }
                    }
                };

                NotificationBusUtil.SendAsync(applicationProfileEventArgs.Configuration, "request_applicationprofile", JArray.FromObject(new List<Database.DataModel.User>() { applicationProfileEventArgs.RequestOwner }), null, args);
            }
        }
    }
}
