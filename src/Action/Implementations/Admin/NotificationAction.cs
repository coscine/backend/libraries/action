﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;

namespace Coscine.Action.Implementations.Admin
{
    public class NotificationAction : IAdminAction
    {
        public int Priority => 1;
        public bool Enabled => true;

        public void OnQutaChanged(AdminEventArgs adminEventArgs)
        {
            NotificationBusUtil.SendAsync(adminEventArgs.Configuration, "quota_changed", NotificationBusUtil.GetAllOwnersForProjectIdPacked(adminEventArgs.ProjectId), adminEventArgs.ProjectId.ToString(), new Newtonsoft.Json.Linq.JObject());
        }
    }
}
