﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Newtonsoft.Json.Linq;

namespace Coscine.Action.Implementations.PID
{
    internal class NotificationAction : IPIDAction
    {
        public int Priority { get { return 1; } }
        public bool Enabled { get { return true; } }

        public void OnPIDOwnerContact(PIDEventArgs pidEventArgs)
        {
            if (!pidEventArgs.SilentMode)
            {
                var args = new JObject()
                {
                    ["Args"] = new JObject() { ["placeholder"] = pidEventArgs.Placeholder }
                };

                var users = new JArray();
                string projectID = "";
                string action = "";
                // contacting owner and creator of resource
                if (pidEventArgs.Resource != null)
                {
                    users = NotificationBusUtil.GetAllOwnersAndCreatorForResourceId(pidEventArgs.Resource.Id);
                   
                    projectID = NotificationBusUtil.GetProjectId(pidEventArgs.Resource.Id).ToString();
                    action = "pid_contact_resource";
                }
                // contacting owner of project
                else if (pidEventArgs.Project != null)
                {
                    users = NotificationBusUtil.GetAllOwnersForProjectIdPacked(pidEventArgs.Project.Id);
                    projectID = pidEventArgs.Project.Id.ToString();
                    action = "pid_contact_project";
                }

                NotificationBusUtil.SendAsync(pidEventArgs.Configuration, action, users, projectID, args);

                if (pidEventArgs.SentCopy)
                {
                    NotificationBusUtil.SendAsync(pidEventArgs.Configuration, "pid_contact_requester", NotificationBusUtil.GetUserList(pidEventArgs.Requester), projectID, args);
                }
            }
        }
    }
}
