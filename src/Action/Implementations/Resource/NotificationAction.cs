﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Newtonsoft.Json.Linq;

namespace Coscine.Action.Implementations.Resource
{
    internal class NotificationAction : IResourceAction
    {
        public int Priority { get { return 1; } }
        public bool Enabled { get { return true; } }

        public void OnResourceCreate(ResourceEventArgs resourceEventArgs)
        {            
            if (!resourceEventArgs.SilentMode)
            {
                var args = new JObject()
                {
                    ["Args"] = new JObject()
                    {
                        ["placeholder"] = new JObject()
                        {
                            ["resourceName"] = resourceEventArgs.Resource.DisplayName
                        }
                    }
                };
                NotificationBusUtil.SendAsync(resourceEventArgs.Configuration, "resource_created", NotificationBusUtil.GetAllUsersForResourceId(resourceEventArgs.Resource.Id), NotificationBusUtil.GetProjectId(resourceEventArgs.Resource.Id).ToString(), args);
            }
        }

        public void OnResourceDelete(ResourceEventArgs resourceEventArgs)
        {
            if (!resourceEventArgs.SilentMode)
            {
                var args = new JObject()
                {
                    ["Args"] = new JObject()
                    {
                        ["placeholder"] = new JObject()
                        {
                            ["resourceName"] = resourceEventArgs.Resource.DisplayName
                        }
                    }
                };
                NotificationBusUtil.SendAsync(resourceEventArgs.Configuration, "resource_deleted", NotificationBusUtil.GetAllUsersForResourceId(resourceEventArgs.Resource.Id), NotificationBusUtil.GetProjectId(resourceEventArgs.Resource.Id).ToString(), args);
            }
        }
    }
}
