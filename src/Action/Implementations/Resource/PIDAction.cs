﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;

namespace Coscine.Action.Implementations.Resource
{
    internal class PIDAction : IResourceAction
    {
        public int Priority { get { return 0; } }
        public bool Enabled { get { return true; } }

        public void OnResourceCreate(ResourceEventArgs resourceEventArgs)
        {
            EpicUtil.CreatePID(resourceEventArgs.Resource.Id.ToString(), resourceEventArgs.Configuration);
        }

        public void OnResourceDelete(ResourceEventArgs resourceEventArgs)
        {
            // A PID is not intended to be ever deleted
        }
    }
}
