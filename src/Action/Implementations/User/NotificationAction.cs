﻿using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Newtonsoft.Json.Linq;

namespace Coscine.Action.Implementations.User
{
    internal class NotificationAction : IUserAction
    {
        public int Priority { get { return 1; } }
        public bool Enabled { get { return true; } }

        public void OnUserSet(UserEventArgs userEventArgs)
        {
            if (!userEventArgs.SilentMode)
            {
                NotificationBusUtil.SendAsync(userEventArgs.Configuration, "user_role_changed", NotificationBusUtil.GetUserList(userEventArgs.User), userEventArgs.Project.Id.ToString(), new JObject());
            }
        }

        public void OnUserDelete(UserEventArgs userEventArgs)
        {
            if (!userEventArgs.SilentMode)
            {
                NotificationBusUtil.SendAsync(userEventArgs.Configuration, "user_deleted", NotificationBusUtil.GetUserList(userEventArgs.User), userEventArgs.Project.Id.ToString(), new JObject());
            }
        }
    }
}
