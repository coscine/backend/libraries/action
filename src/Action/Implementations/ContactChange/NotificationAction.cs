using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Coscine.Database.Models;
using Newtonsoft.Json.Linq;

namespace Coscine.Action.Implementations.ContactChange
{
    public class NotificationAction : IContactChangeAction
    {
        public int Priority { get { return 1; } }
        public bool Enabled { get { return true; } }

        public void OnContactChange(ContactChangeEventArgs contactChangeEventArgs)
        {
            if (!contactChangeEventArgs.SilentMode)
            {
                var baseURL = $"{contactChangeEventArgs.Configuration.GetStringAndWait("coscine/local/api/additional/url")}";
                var completeURL = $"{baseURL}/?emailtoken={contactChangeEventArgs.ContactInformation.ConfirmationToken}";
                var args = new JObject()
                {
                    ["Args"] = new JObject()
                    {
                        ["placeholder"] = new JObject()
                        {
                            ["confirmation_link"] = completeURL
                        }
                    }
                };
                var user = new UserModel().GetById(contactChangeEventArgs.ContactInformation.UserId);
                user.EmailAddress = contactChangeEventArgs.ContactInformation.NewEmail;
                const string projectID = null;
                const string action = "user_email_changed";

                NotificationBusUtil.SendAsync(contactChangeEventArgs.Configuration, action, NotificationBusUtil.GetUserList(user), projectID, args);
            }
        }
    }
}
