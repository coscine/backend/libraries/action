﻿using Coscine.Action.EventArgs;

namespace Coscine.Action.Implementations.Metadata
{
    public class SemanticSearchAction : IMetadataAction
    {
        public int Priority => 1;

        //Activated this again!
        public bool Enabled => false;

        public void OnMetadataCreate(MetadataEventArgs args)
        {
            //var metadataStore = args.Configuration.GetString("coscine/local/virtuoso/additional/url");
            //var elasticsearchServer = args.Configuration.GetString("coscine/local/elasticsearch/additional/server");
            //var elasticsearchPort = args.Configuration.GetString("coscine/local/elasticsearch/additional/port");
            //var connector = new VirtuosoRdfConnector(metadataStore);
            //var searchClient = new ElasticsearchSearchClient(elasticsearchServer, elasticsearchPort);
            //var searchMapper = new RdfSearchMapper(connector, searchClient, args.DefaultLanguages);
            //searchMapper.AddDocumentAsync(args.GraphName).Wait();
        }

        public void OnMetadataDelete(MetadataEventArgs args)
        {
            //var metadataStore = args.Configuration.GetString("coscine/local/virtuoso/additional/url");
            //var elasticsearchServer = args.Configuration.GetString("coscine/local/elasticsearch/additional/server");
            //var elasticsearchPort = args.Configuration.GetString("coscine/local/elasticsearch/additional/port");
            //var connector = new VirtuosoRdfConnector(metadataStore);
            //var searchClient = new ElasticsearchSearchClient(elasticsearchServer, elasticsearchPort);
            //var searchMapper = new RdfSearchMapper(connector, searchClient, args.DefaultLanguages);
            //searchMapper.DeleteDocumentAsync(args.GraphName).Wait();
        }

        public void OnMetadataUpdate(MetadataEventArgs args)
        {
            //var metadataStore = args.Configuration.GetString("coscine/local/virtuoso/additional/url");
            //var elasticsearchServer = args.Configuration.GetString("coscine/local/elasticsearch/additional/server");
            //var elasticsearchPort = args.Configuration.GetString("coscine/local/elasticsearch/additional/port");
            //var connector = new VirtuosoRdfConnector(metadataStore);
            //var searchClient = new ElasticsearchSearchClient(elasticsearchServer, elasticsearchPort);
            //var searchMapper = new RdfSearchMapper(connector, searchClient, args.DefaultLanguages);
            //searchMapper.DeleteDocumentAsync(args.GraphName).Wait();
            //searchMapper.AddDocumentAsync(args.GraphName).Wait();
        }
    }
}
