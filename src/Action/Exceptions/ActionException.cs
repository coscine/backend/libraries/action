﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Action.Exceptions
{
    public class ActionException : Exception
    {
        public ActionException(string message) : base(message)
        {
        }

        public ActionException() : base()
        {
        }

        public ActionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
