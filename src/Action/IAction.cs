﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Action
{
    public interface IAction 
    {
        int Priority { get; }

        bool Enabled { get; }
    }
}
