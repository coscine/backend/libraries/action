﻿using Coscine.Action.EventArgs;

namespace Coscine.Action
{
    public interface IResourceAction : IAction
    {
        void OnResourceCreate(ResourceEventArgs resourceEventArgs);
        void OnResourceDelete(ResourceEventArgs resourceEventArgs);
    }
}
